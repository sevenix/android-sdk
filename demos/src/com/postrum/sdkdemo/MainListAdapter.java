package com.postrum.sdkdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.postrum.sdk.PBeacon;
import com.postrum.sdk.PBeaconManager;
import com.postrum.sdk.PBeaconRegion;

import java.util.ArrayList;

/*
 * Copyright 2014 Valentin Barral <valentin.barral@postrum.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MainListAdapter  extends BaseExpandableListAdapter{

    private ArrayList<ScanResult> scanResults;
    private Context context;



    public MainListAdapter(Context context){
        this.scanResults = new ArrayList<ScanResult>();
        this.context = context;
    }


    public void clear(){
        scanResults.clear();
    }

    public void addMonitoring(PBeaconRegion region, boolean isInside){
        this.scanResults.add(new ScanResult(region, isInside));
        this.notifyDataSetChanged();

    }

    public void addRegioning(PBeaconRegion region, ArrayList<PBeacon> beacons){
        this.scanResults.add(new ScanResult(region, beacons));
        this.notifyDataSetChanged();
    }


    @Override
    public int getGroupCount() {
        return scanResults.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return scanResults.get(i).getBeacons().size();
    }

    @Override
    public Object getGroup(int i) {
        return scanResults.get(i);
    }

    @Override
    public Object getChild(int i, int i2) {
        return scanResults.get(i).getBeacons().get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i2) {
        return  i2;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        ScanResult data = scanResults.get(i);
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.main_row, null);
        }

        TextView textViewProximityUUID= (TextView) view.findViewById(R.id.textViewMainRowProximityUUID);
        textViewProximityUUID.setText(data.getRegion().getProximityUUID());

        TextView textViewMajor= (TextView) view.findViewById(R.id.textViewMainRowMajor);
        if (data.getRegion().getMajor()==null){
            textViewMajor.setText("-");
        } else {
            textViewMajor.setText(data.getRegion().getMajor().toString());
        }

        TextView textViewMinor= (TextView) view.findViewById(R.id.textViewMainRowMinor);
        if (data.getRegion().getMinor()==null){
            textViewMinor.setText("-");
        } else {
            textViewMinor.setText(data.getRegion().getMinor().toString());
        }

        ImageView imageView = (ImageView) view.findViewById(R.id.imageViewMainRow);

        if (data.getResultType().equalsIgnoreCase(PBeaconManager.TYPE_RANGING)){
            //Ranging image
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_ranging));
        } else if (data.getResultType().equalsIgnoreCase(PBeaconManager.TYPE_MONITORING)){
            //Monitoring
                if (data.isInside()){
                    //Enters
                    imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_monitoring_enter));
                } else {
                    //Exit
                    imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_monitoring_exit));
                }
        }

        return view;

    }

    @Override
    public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {
       PBeacon beacon = scanResults.get(i).getBeacons().get(i2);

        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.detail_row, null);
        }

        TextView textViewProximityUUID= (TextView) view.findViewById(R.id.textViewDetailRowProximityUUID);
        textViewProximityUUID.setText(beacon.getProximityUUID());

        TextView textViewMajor= (TextView) view.findViewById(R.id.textViewDetailRowMajor);
        if (beacon.getMajor()==null){
            textViewMajor.setText("-");
        } else {
            textViewMajor.setText(beacon.getMajor().toString());
        }

        TextView textViewMinor= (TextView) view.findViewById(R.id.textViewDetailRowMinor);
        if (beacon.getMinor()==null){
            textViewMinor.setText("-");
        } else {
            textViewMinor.setText(beacon.getMinor().toString());
        }

        TextView textViewMac= (TextView) view.findViewById(R.id.textViewDetailRowMac);
        if (beacon.getDevice()!=null){
            textViewMac.setText(beacon.getDevice().getAddress());
        } else {
            textViewMac.setText("-");
        }


        TextView textViewProximity= (TextView) view.findViewById(R.id.textViewDetailRowProximity);
        textViewProximity.setText(this.getStringFromProximity(beacon.getProximity()));

        TextView textViewRss= (TextView) view.findViewById(R.id.textViewDetailRowRss);
        textViewRss.setText(beacon.getRssi().toString());

        ImageView imageView = (ImageView) view.findViewById(R.id.imageViewDetailView);
        imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_beacon));

        return view;

    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

    private String getStringFromProximity(Integer proximity){
        switch (proximity){
            case PBeacon.PROXIMITY_FAR:
                return "FAR";
            case PBeacon.PROXIMITY_IMMEDIATE:
                return "IMMEDIATE";
            case PBeacon.PROXIMITY_NEAR:
                return "NEAR";
            case PBeacon.PROXIMITY_UNKNOWN:
                return "UNKNOWN";
            default:
                return "-";
        }
    }
}
