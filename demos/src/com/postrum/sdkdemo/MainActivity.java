package com.postrum.sdkdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.postrum.sdk.PBeacon;
import com.postrum.sdk.PBeaconManager;
import com.postrum.sdk.PBeaconRegion;

import java.util.ArrayList;

/*
 * Copyright 2014 Valentin Barral <valentin.barral@postrum.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class MainActivity extends Activity implements PBeaconManager.PBeaconServiceDelegate, PBeaconManager.MonitoringDelegate, PBeaconManager.RangingDelegate {

    private static int MONITORING = 0;
    private static int RANGING = 1;

    private Switch switchMonitoring;
    private Switch switchRanging;


    private EditText editTextProximityUUID;
    private EditText editTextMajor;
    private EditText editTextMinor;

    private ScrollView scrollView;

    private PBeaconRegion currentRegion;

    private ExpandableListView listView;
    private MainListAdapter mainListAdapter;


    private boolean connected;
    private int operationAfterConnect;
    private boolean monitoringOn;
    private boolean rangingOn;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        connected = false;
        operationAfterConnect = MONITORING;
        rangingOn = false;
        monitoringOn = false;

        switchMonitoring = (Switch) findViewById(R.id.switchMonitoring);
        switchMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((Switch) view).isChecked()) {
                    startMonitoring();
                } else {
                    stopMonitoring();
                }
            }
        });
        switchRanging = (Switch) findViewById(R.id.switchRanging);
        switchRanging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((Switch) view).isChecked()) {
                    startRanging();
                } else {
                    stopRanging();
                }
            }
        });

        editTextProximityUUID = (EditText) findViewById(R.id.editTextProximityUUID);
        editTextMajor = (EditText) findViewById(R.id.editTextMajor);
        editTextMinor = (EditText) findViewById(R.id.editTextMinor);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        listView = (ExpandableListView) findViewById(R.id.expandableListViewMain);
        mainListAdapter = new MainListAdapter(getBaseContext());
        listView.setAdapter(mainListAdapter);


        PBeaconManager.getInstance(getBaseContext()).setMonitoringDelegate(this);
        PBeaconManager.getInstance(getBaseContext()).setRangingDelegate(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.main_menu_clear:
                clear();
                return true;
            default:
                return true;
        }
    }


    private void clear(){
        mainListAdapter.clear();
        mainListAdapter.notifyDataSetChanged();
    }


    private void connectPBeaconService() {
        PBeaconManager.getInstance(getBaseContext()).connectToPBeaconService(this, this);
    }

    private void disconnectPBeaconService() {
        PBeaconManager.getInstance(getBaseContext()).disconnectFromPBeaconService();

    }

    private void startMonitoring() {
        if (editTextProximityUUID.getText().length() == 0) {
            //Error, a ProximityUUID is needed
            switchMonitoring.setChecked(false);
            showToastMessage("A ProximityUUID is needed!");
        } else {
            if (connected) {
                Integer major = null;
                Integer minor = null;

                try {
                    major = Integer.valueOf(editTextMajor.getText().toString());
                } catch (NumberFormatException ex) {
                    //We dont use major
                }

                try {
                    minor = Integer.valueOf(editTextMinor.getText().toString());
                } catch (NumberFormatException ex) {
                    //We dont use major
                }

                currentRegion = new PBeaconRegion("my_region", editTextProximityUUID.getText().toString(), major, minor);

                try {
                    PBeaconManager.getInstance(getBaseContext()).startMonitoring(currentRegion);
                    monitoringOn = true;
                    setEnabledRegionValues(false);
                } catch (PBeaconManager.PBeaconManagerException ex) {
                    monitoringOn = false;
                    switchMonitoring.setChecked(false);
                    showToastMessage(ex.getMessage());
                    setEnabledRegionValues(true);
                }


            } else {
                //Not connected, we've to start service
                operationAfterConnect = MONITORING;
                connectPBeaconService();
            }
        }

    }


    private void stopMonitoring() {
        if (currentRegion != null) {
            try {
                PBeaconManager.getInstance(getBaseContext()).stopMonitoring(currentRegion);
                monitoringOn = false;
            } catch (PBeaconManager.PBeaconManagerException ex) {
                switchMonitoring.setChecked(false);
                showToastMessage(ex.getMessage());
            }
        }
        disconnectPBeaconServiceIfNeeded();
    }


    private void startRanging() {
        if (editTextProximityUUID.getText().length() == 0) {
            //Error, a ProximityUUID is needed
            switchRanging.setChecked(false);
            showToastMessage("A ProximityUUID is needed!");
        } else {
            if (connected) {
                Integer major = null;
                Integer minor = null;

                try {
                    major = Integer.valueOf(editTextMajor.getText().toString());
                } catch (NumberFormatException ex) {
                    //We dont use major
                }

                try {
                    minor = Integer.valueOf(editTextMinor.getText().toString());
                } catch (NumberFormatException ex) {
                    //We dont use major
                }

                currentRegion = new PBeaconRegion("my_region", editTextProximityUUID.getText().toString(), major, minor);

                try {
                    PBeaconManager.getInstance(getBaseContext()).startRanging(currentRegion);
                    setEnabledRegionValues(false);
                    rangingOn = true;
                } catch (PBeaconManager.PBeaconManagerException ex) {
                    switchRanging.setChecked(false);
                    rangingOn = false;
                    showToastMessage(ex.getMessage());
                    setEnabledRegionValues(true);
                }


            } else {
                //Not connected
                operationAfterConnect = RANGING;
                connectPBeaconService();
            }
        }
    }

    private void stopRanging() {
        if (currentRegion != null) {
            try {
                PBeaconManager.getInstance(getBaseContext()).stopRanging(currentRegion);
                rangingOn = false;
            } catch (PBeaconManager.PBeaconManagerException ex) {
                showToastMessage(ex.getMessage());
            }
        }

        disconnectPBeaconServiceIfNeeded();
    }


    private void disconnectPBeaconServiceIfNeeded(){
        if (!rangingOn && !monitoringOn){
            disconnectPBeaconService();
        }
    }
    private void showToastMessage(CharSequence message) {
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(getBaseContext(), message, duration);
        toast.show();
    }

    private void setEnabledRegionValues(boolean enabled) {
        editTextProximityUUID.setEnabled(enabled);
        editTextMajor.setEnabled(enabled);
        editTextMinor.setEnabled(enabled);
    }

    //PBeaconServiceDelegate methods
    @Override
    public void onPBeaconServiceConnect() {
        showToastMessage("Service bounded!");
        connected = true;
        if (operationAfterConnect == MONITORING) {
            startMonitoring();
        } else {
            startRanging();
        }
    }

    @Override
    public void onPBeaconServiceDisconnect() {
        showToastMessage("Service unbounded.");
        connected = false;
        setEnabledRegionValues(true);
    }

    @Override
    public void onPBeaconServiceError() {
        showToastMessage("Service connection error.");
        connected = false;
    }


    //PBeaconManager.MonitoringDelegate methods
    @Override
    public void enteredRegion(PBeaconRegion region) {
        final PBeaconRegion finalRegion = new PBeaconRegion(region);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainListAdapter.addMonitoring(finalRegion, true);
                scrollMyListViewToBottom();
            }
        });
    }

    @Override
    public void leftRegion(PBeaconRegion region) {
        final PBeaconRegion finalRegion = new PBeaconRegion(region);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainListAdapter.addMonitoring(finalRegion, false);
                scrollMyListViewToBottom();
            }
        });

    }


    //PBeaconManager.RangingDelegate methods
    @Override
    public void newRangingInRegion(ArrayList<PBeacon> beacons, PBeaconRegion region) {
        final ArrayList<PBeacon> finalBeacons = new ArrayList<PBeacon>(beacons);
        final PBeaconRegion finalRegion = new PBeaconRegion(region);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainListAdapter.addRegioning(finalRegion, finalBeacons);
                scrollMyListViewToBottom();
            }
        });

    }


    private void scrollMyListViewToBottom() {
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setSelection(mainListAdapter.getGroupCount() - 1);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (connected) {
        PBeaconManager.getInstance(getBaseContext()).disconnectFromPBeaconService();
        }
    }
}