package com.postrum.sdkdemo;

import com.postrum.sdk.PBeacon;
import com.postrum.sdk.PBeaconManager;
import com.postrum.sdk.PBeaconRegion;

import java.util.ArrayList;

/*
 * Copyright 2014 Valentin Barral <valentin.barral@postrum.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class ScanResult {

    private String resultType;
    private PBeaconRegion region;
    private boolean inside;
    private ArrayList<PBeacon> beacons;


    public ScanResult(PBeaconRegion region, boolean inside) {
        this.region = region;
        this.inside = inside;
        this.beacons = new ArrayList<PBeacon>();
        this.resultType = PBeaconManager.TYPE_MONITORING;
    }


    public ScanResult(PBeaconRegion region, ArrayList<PBeacon> beacons) {
        this.region = region;
        this.beacons = new ArrayList<PBeacon>(beacons);
        this.inside = true;
        this.resultType = PBeaconManager.TYPE_RANGING;

    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public PBeaconRegion getRegion() {
        return region;
    }

    public void setRegion(PBeaconRegion region) {
        this.region = region;
    }

    public boolean isInside() {
        return inside;
    }

    public void setInside(boolean inside) {
        this.inside = inside;
    }

    public ArrayList<PBeacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(ArrayList<PBeacon> beacons) {
        this.beacons = beacons;
    }
}
