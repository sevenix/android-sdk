# Postrum SDK for Android #

## Overview ##

Postrum SDK for Android is the library needed to work with Postrum beacons (PBeacons).

### System Requirements ###

Postrum SDK for Android needs a device with Android 4.3 (minSdkVersion="18" on `AndroidManifest.xml`) or above with Bluetooth Low Energy (BLE) capabilities. 

### Postrum Beacons ecosystem ###

Postrum SDK for Android works with the next main entities:

- PBeacon
- PBeaconRegion
- PBeaconManager
- PBeaconScanner

** PBeacon **

A PBeacon object represents a physical Postrum beacon. It contains all its properties and attributes, such as identifiers, transmit power, distance to user, advertising intervals, etc. A PBeacon is identified by tree main attributes: a Proximity UUID, a Major and a Minor. The Proximity UUID is a proximity identifier, and it has the XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX format, 16-byte long represented in hexadecimal format. The Major and Minor are two optional attributes, 16-bit unsigned integers that the user can set to define custom regions.

** PBeaconRegion **

A PBeaconRegion is a logical definition of a set of PBeacons. It also has a Proximity UUID, Major and Minor attributes, and the main idea is that a PBeacon is _inside_ a PBeaconRegion when it matches the PBeacon's Proximity UUID, Major and/or Minor corresponding to the PBeaconRegion parameters. Note that, as the Major and Minor are optional attributes, the matching will apply only to the set parameters.

** PBeaconManager**

The PBeaconManager offers the following operations:

1. Ranging: a ranging operation provides distance information between the user device and the PBeacons. When the user starts a ranging process, he is notified every time new PBeacons are detected or when there are changes in their distance values.

2. Monitoring: a monitoring operation gives information about when the user device enters or leaves a PBeaconRegion. When the user sets a desired region of interest, the PBeaconManager will automatically notify him every time a PBeacon enters or exits that region.

3. Connection with a PBeacon: PBeaconManager allows the user to establish a connection with a PBeacon, and get and set some of its internal parameters, such as the Major, Minor, Transmit Power, Advertising period, etc.

** PBeaconScanner **

The Ranging and Monitoring are "subscription" operations, that is, the user applications register themselves as observers and they receive notifications when new events occur. However, sometimes the applications may need to make specific scans (BLE discovery) by demand at a desired time, so that they can use the PBeaconScanner class. This class allows to configure and launch a specific scan process on demand (setting its scan duration, regions of interest, etc.).


### Postrum SDK Installation ###

1. Copy PostrumAndroidSDK.jar to your `libs` directory.
2. Set the following permissions to the `AndroidManifest.xml`:
```xml
<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
```
3. Add the following service declarations to the `AndroidManifest.xml`: 
```xml
<service android:enabled="true" android:name="com.postrum.sdk.PBeaconService" />
<service android:enabled="true" android:name="com.postrum.sdk.PBeaconServiceMessageReceiver" />
```

## Usage examples ##

1. Import `demos/src` and `demos/res` directories to a new Android project.
2. Copy `demos/AndroidManifest.xml` file to your Android project.
3. Copy PostrumAndroidSDK.jar to your `libs` directory.
4. Add PostrumAndroidSDK.jar as library to your Android project.
5. Compile and run.

### Ranging example ###

```java
// Before you start, set your desired Proximity UUID here:
private static final String POSTRUM_PROXIMITY_UUID = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
private PBeaconRegion myRegion = new Region("myRegion", POSTRUM_PROXIMITY_UUID, 0, null);

// Set a Ranging delegate
PBeaconManager.getInstance(getBaseContext()).setRangingDelegate(new PBeaconManager.RangingDelegate() {
    @Override
    public void newRangingInRegion(ArrayList<PBeacon> beacons, PBeaconRegion region) {
        // New beacons detected!
        Iterator<PBeacon> beaconsIterator = beacons.iterator();
        while (beaconsIterator.hasNext()){
            System.out.println("PBeacon with proximity: " + beaconsIterator.next().getProximity());
        }
    }
});

// A main method
private void mainMethod() {
    // Connect with service
    PBeaconManager.getInstance(getBaseContext()).connectToPBeaconService(new PBeaconManager.PBeaconServiceDelegate() {
        @Override
        public void onPBeaconServiceConnect() {
            // We are connected and we can start launching operations to the PBeacon
            startRanging();
        }

        @Override
        public void onPBeaconServiceError() {
            // Something bad happened :(
        }
    }, this);
}

// Start the ranging process
private void startRanging(){
    try {
        PBeaconManager.getInstance(getBaseContext()).startRanging(myRegion);
    } catch (PBeaconManager.PBeaconManagerException ex) {
        showToastMessage(ex.getMessage());
    }
}

// Stop the ranging process
private void stopRanging(){
    try {
		// Stop ranging
        PBeaconManager.getInstance(getBaseContext()).stopRanging(myRegion);
		// Disconnect from service
		PBeaconManager.getInstance(getBaseContext()).disconnectFromPBeaconService();
    } catch (PBeaconManager.PBeaconManagerException ex) {
        showToastMessage(ex.getMessage());
    }
}

```
### Monitoring example ###

```java
// Before you start, set your desired Proximity UUID here:
private static final String POSTRUM_PROXIMITY_UUID = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
private PBeaconRegion myRegion = new Region("myRegion", POSTRUM_PROXIMITY_UUID, 0, null);

// Set a Monitoring delegate
PBeaconManager.getInstance(getBaseContext()).setMonitoringDelegate(new PBeaconManager.MonitoringDelegate() {
    @Override
    public void enteredRegion(PBeaconRegion region) {
        System.out.println("Entered region " + region.getId());
    }

    @Override
    public void leftRegion(PBeaconRegion region) {
        System.out.println("Left region " + region.getId());
    }
});

// A main method
private void mainMethod() {
    // Connect with service
    PBeaconManager.getInstance(getBaseContext()).connectToPBeaconService(new PBeaconManager.PBeaconServiceDelegate() {
        @Override
        public void onPBeaconServiceConnect() {
            // We are connected and we can start launching operations to the PBeacon
			startMonitoring();
        }

        @Override
        public void onPBeaconServiceError() {
            // Something bad happened :(
        }
    }, this);
}

// Start monitoring
private void startMonitoring(){
    try {
        PBeaconManager.getInstance(getBaseContext()).startMonitoring(myRegion);
    } catch (PBeaconManager.PBeaconManagerException ex) {
        showToastMessage(ex.getMessage());
    }
}

// Stop monitoring
private void stopMonitoring() {
    try {
		// Stop monitoring
        PBeaconManager.getInstance(getBaseContext()).stopMonitoring(myRegion);
		// Disconnect from service
		PBeaconManager.getInstance(getBaseContext()).disconnectFromPBeaconService();
    } catch (PBeaconManager.PBeaconManagerException ex) {
        showToastMessage(ex.getMessage());
    }
}

```

### Scanner example ###

```java
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
    mBluetoothAdapter = bluetoothManager.getAdapter();

    long scanWindow = 2000;       // Discovery total timeout [millis]
    long singleScanWindow = 500;  // Single discovery window [millis]
    PBeaconScanner pBeaconScanner = new PBeaconScanner(mBluetoothAdapter, new PBeaconScanner.PBeaconScannerDelegate() {
        @Override
        public void endScan(ArrayList<PBeacon> beacons) {
            Iterator<PBeacon> beaconsIterator = beacons.iterator();
            while (beaconsIterator.hasNext()){
                System.out.println("PBeacon with proximity: " + beaconsIterator.next().getProximity());
            }
        }
    }, scanWindow, singleScanWindow);
	
	pBeaconScanner.startScan();
} 

```
## Changelog ##

* Beta 0.1 (March 28, 2014)
* Initial version.
